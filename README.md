**This repository is part of a larger experiment to build a geoscientific data catalog using modern cloud architectures with a special focus on collecting all data types within a single system to break down silos.**

**The project is stable but not production-ready and no longer maintained in its current form. It's released to inspire others and showcase how to build such a platform. See [getgeodata](getgeodata.com) for more.**

**If you have technical questions about the design (not the installation or setup) you are welcome to contact hello@getgeodata.com**

# geodatahublib

Client libraries for interacting with [GeoDataHub]. Install with pip,

`pip install geodatahub`


## Client library variants contained in this repository
- `geodatahub.py/`: Python module for interfacing with the [GeoDataHub] backend.
- `docs/`: Old documentation scheduled to be removed

