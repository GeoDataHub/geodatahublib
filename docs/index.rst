GeoDataHub Documentation
-------------------------

These pages contain usage information for the [GeoDataHub] client libraries and 
plugins. Foo bar

Table of content
------------------------
.. toctree::
   :maxdepth: 1

   apis/geodatahublib

[GeoDataHub]: https://geodatahub.dk
