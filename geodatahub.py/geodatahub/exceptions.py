class ConnectionError(Exception):
    '''
    Raised to signal a problem with the GeoDataHub connection.
    '''
