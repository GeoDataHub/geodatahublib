# GeoDataHub Python client library
This library allows for two-way communication with the [GeoDataHub] backend.

## License
Apache License version 2, see `LICENSE.txt`

## Installing
The latest release of the geodatahub python module can be installed with the 
command:

    pip install geodatahub

## Authors
The [GeoDataHub] team, [info@geodatahub.dk](mailto:info@geodatahub.dk).

[GeoDataHub]: https://geodatahub.dk
